<?php
/**
 * @file Token API
 */

/**
 * Implementation of hook_token_values().
 */
//TODO: check this, since the sanitize function is no more:
/*
function audio_assist_token_values($type, $object = NULL, $options = array()) {
  $values = array();
  switch ($type) {
    case 'node':
      $node = $object;
      $values['teaser'] = audio_assist_sanitize($node->teaser);
      $values['body']   = audio_assist_sanitize($node->body);
      break;
  }
  return $values;
}
*/
/**
 * Implementation of hook_token_list().
 */
function audio_assist_token_list($type = 'all') {
  if ($type == 'node' || $type == 'all') {
    $tokens['node']['teaser'] = t('Node teaser text (plain text)');
    $tokens['node']['body']   = t('Node body text (plain text)');
    return $tokens;
  }
}


<?php

/**
 * @file
 * Audio Assist module
 */

/**
 * Implementation of hook_help().
 */
function audio_assist_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/audio_assist':
      return t('If this site was moved or is planned to move to another domain or sub-directory, it might be needed to <a href="!empty-cache">empty the filter cache</a> to correct image paths that are pointing to the old address.  Note that this will only work for images that have been inserted using filter tags.', array('!empty-cache' => url('audio_assist/cache/clear')));

    case 'audio_assist/template':
      return '<div class="%audio-class"><a href="%link">%audio</a><div class="caption">%caption</div></div>';
  }
}

/**
 * Implementation of hook_theme().
 */
function audio_assist_theme() {
  return array(
    'audio_assist_inline' => array(
      'arguments' => array('node' => NULL, 'size' => NULL, 'attributes' => NULL),
    ),
    'audio_assist_filter' => array(
      'arguments' => array('text' => NULL),
    ),
    'audio_assist_popup' => array(
      'arguments' => array('content' => NULL, 'attributes' => NULL),
    ),
    'audio_assist_page' => array(
      'arguments' => array('content' => NULL, 'attributes' => NULL),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function audio_assist_menu() {
  $items = array();


  $items['audio_assist/cache/clear'] = array(
    'title' => t('Empy Cache'),
    'page callback' => 'audio_assist_cache_clear',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['audio_assist/load'] = array(
    'title' => 'Audio assist',
    'page callback' => 'audio_assist_loader',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  // Page callbacks called internally by audio_assist/load.
  $items['audio_assist/header'] = array(
    'title' => 'Audio assist header',
    'page callback' => 'audio_assist_header',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['audio_assist/thumbs'] = array(
    'title' => 'Audio assist thumbnails',
    'page callback' => 'audio_assist_thumbs',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['audio_assist/properties'] = array(
    'title' => 'Audio assist properties',
    'page callback' => 'audio_assist_properties',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  // Popup images page.
  $items['audio_assist/popup'] = array(
    'title' => 'Popup image',
    'page callback' => 'audio_assist_popup',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  // Insert callback (only for inserting HTML, not filter tag).
  $items['audio_assist/insert_html'] = array(
    'title' => 'Insert callback',
    'page callback' => 'audio_assist_insert_html',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/audio_assist'] = array(
    'title' => 'Audio assist',
    'description' => 'Change settings for the Audio assist module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('audio_assist_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}

/**
 * Implementation of hook_init().
 */
function audio_assist_init() {
  $path = drupal_get_path('module', 'audio_assist');
  if (arg(0) == 'audio_assist') {
    // Suppress Administration menu.
    module_invoke('admin_menu', 'suppress');

    drupal_add_css($path .'/audio_assist_popup.css', 'module', 'all', FALSE);
  }
  else {
    drupal_add_js($path .'/audio_assist.js');
    if (variable_get('audio_assist_page_styling', 'yes') == 'yes') {
      drupal_add_css($path .'/audio_assist.css');
    }
  }
}

/**
 * Implementation of hook_perm().
 */
function audio_assist_perm() {
  return array('access audio_assist', 'access advanced options' );
}

/**
 * Implementation of hook_elements().
 */
function audio_assist_elements() {
  $type['textarea'] = array(
    '#process' => 'audio_assist_textarea'
  );
  return $type;
}

/**
 * Add JavaScript settings for generating the audio link underneath textareas.
 */
function audio_assist_textarea($element) {
  static $initialized = FALSE;

  if (!user_access('access audio_assist')) {
    return $element;
  }
  $link = variable_get('audio_assist_link', 'icon');
  if ($link == 'icon' || $link == 'text') {
    if (_audio_assist_textarea_match($element['#id']) && _audio_assist_page_match() && !strstr($_GET['q'], 'audio_assist')) {
      if (!$initialized) {
        // Add settings.
        $settings['link'] = $link;
        if ($link == 'icon') {
          $settings['icon'] = drupal_get_path('module', 'audio_assist') .'/add-audio.png';
        }
        drupal_add_js(array('audio_assist' => $settings), 'setting');
        $initialized = TRUE;
      }

      // Attach behavior.
      // @todo Some browsers do not support underscores in CSS classes.
      if (!isset($element['#attributes']['class'])) {
        $element['#attributes']['class'] = 'audio_assist';
      }
      else {
        $element['#attributes']['class'] .= ' audio_assist';
      }
    }
  }
  return $element;
}

/**
 * Implementation of hook_settings().
 */
function audio_assist_admin_settings() {
  require_once drupal_get_path('module', 'audio_assist') .'/includes/audio_assist.token.inc';

  // Access settings.
  $form['access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['access']['audio_assist_paths_type'] = array(
    '#type' => 'radios',
    '#title' => t('Display Audio assist on paths'),
    '#default_value' => variable_get('audio_assist_paths_type', 2),
    '#options' => array(t('on specific paths'), t('not on specific paths'), t('all paths')),
  );
  $form['access']['audio_assist_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#default_value' => variable_get('audio_assist_paths', "node/*\ncomment/*"),
    '#cols' => 40,
    '#rows' => 5,
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );
  $form['access']['audio_assist_textareas_type'] = array(
    '#type' => 'radios',
    '#title' => t('Display Audio assist on text areas'),
    '#default_value' => variable_get('audio_assist_textareas_type', 2),
    '#options' => array(t('Show on every textarea except the listed textareas.'), t('Show on only the listed textareas.'), t('Show on all textareas.')),
  );
  $form['access']['audio_assist_textareas'] = array(
    '#type' => 'textarea',
    '#title' => t('Text areas'),
    '#default_value' => variable_get('audio_assist_textareas', "edit-body\nedit-comment"),
    '#cols' => 40,
    '#rows' => 5,
    '#description' => t("Enter one text area form-id per line. Form-id's are used by Drupal to typify them, which allows themers and coders to modify certain form fields, but not all. Find form-id's using this method: view the source of the webpage, then search for the string that's just above the text area and you'll see the form-id nearby. The '*' character is a wildcard. For example, you can specify all CCK fields as %cck-example.", array('%cck-example' => 'edit-field-*')),
  );
  $form['access']['audio_assist_link'] = array(
    '#type' => 'select',
    '#title' => t('Textarea audio link'),
    '#default_value' => variable_get('audio_assist_link', 'icon'),
    '#options' => array('icon' => t('Show icon'), 'text' => t('Show text link'), 'none' => t('Do not show a link')),
    '#description' => t('Choose what to show under the textareas for which Audio assist is enabled.'),
  );


  return system_settings_form($form);
}

/**
 * Implementation of hook_filter().
 TODO: Implemente filter or just full HTML?
 */
function audio_assist_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('Inline images'));
    
    case 'description':
      return t('Add images to your posts with Image assist.');
    
    case 'process':
      $processed = FALSE;
      foreach (audio_assist_get_macros($text) as $unexpanded_macro => $macro) {
        $expanded_macro = audio_assist_render_player($macro);
        $text           = str_replace($unexpanded_macro, $expanded_macro, $text);
        $processed      = TRUE;
      }
      return $processed ? theme('audio_assist_filter', $text) : $text;
    
    default:
      return $text;
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function audio_assist_filter_tips($delta, $format, $long = FALSE) {
  return t('Images can be added to this post.');
}


/**
 * Menu callback; clears relevant caches, redirecting to previous page
 */
function audio_assist_cache_clear() {
  $core = array('cache_filter', 'cache_page');
  foreach ($core as $table) {
    cache_clear_all('*', $table, TRUE);
  }
  drupal_set_message(t('Cache cleared.'));
  drupal_goto('admin/settings/audio_assist');
}


/**
 * @defgroup audio_assist_pages Image Assist Pages
 * @{
 * All but audio_assist_loader() are in frames.
 */

/**
 * Output main audio_assist interface HTML.
 * 
 * @todo Remove hard-coded TinyMCE integration.
 */
function audio_assist_loader() {
  $path = drupal_get_path('module', 'audio_assist');
  $caller = arg(2) ? arg(2) : 'textarea';

  drupal_add_js($path .'/audio_assist_popup.js');
  if (module_exists('wysiwyg') && ($editor = wysiwyg_get_editor($caller))) {
    if ($editor['name'] == 'tinymce') {
      drupal_add_js($editor['library path'] .'/tiny_mce_popup.js');
    }
  }
  elseif (module_exists('tinymce') && $caller == 'tinymce') {
    drupal_add_js(drupal_get_path('module', 'tinymce') .'/tinymce/jscripts/tiny_mce/tiny_mce_popup.js');
  }
  else {
    $caller = 'textarea';
  }
  drupal_add_js($path .'/audio_assist_'. $caller .'.js');

  $output  = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN">'."\n";
  $output .= "<html>\n";
  $output .= "<head>\n";
  $output .= '<title>'. t('Add audio') ."</title>\n";
  $output .= drupal_get_js();
  $output .= "</head>\n\n";

  $output .= '<frameset rows="38, *" onload="initLoader()" frameborder="0" border="0" framespacing="0">'."\n";
  $output .= '<frame name="audio_assist_header" src="" class="audio_assist_header" noresize="noresize" />'."\n";
  $output .= '<frame name="audio_assist_main" src="" class="audio_assist_main" noresize="noresize" />'."\n";
  $output .= "</frameset>\n";
  
  $output .= "</html>\n";
  echo $output;
  exit;
}

/**
 * Fetch the number of nodes for terms of given vocabulary ids.
 *
 * Note that this does not count nodes of subterms, as opposed to 
 * taxonomy_term_count_nodes().
 *
 * @param $vids
 *   An array of taxonomy vocabulary ids.
 *
 * @return
 *   An array keyed by term ids with the count of nodes for each term.
 */
function _audio_assist_term_count_nodes($vids) {
  $count = array();
  $result = db_query(db_rewrite_sql('SELECT t.tid, COUNT(n.nid) AS count FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {term_data} td ON td.tid = t.tid WHERE n.status = 1 AND td.vid IN ('. db_placeholders($vids) .') GROUP BY t.tid'), implode(',', $vids));
  while ($term = db_fetch_object($result)) {
    $count[$term->tid] = $term->count;
  }
  return $count;
}

function audio_assist_header($mode) {
  // Mode may be 'browse'.
  $output = drupal_get_form('audio_assist_header_form');
  echo theme('audio_assist_page', $output, array('id' => 'audio_assist_header', 'onload' => 'parent.initHeader();', 'class' => 'audio_assist'));
  exit;
}

function audio_assist_header_form(&$form_state) {
  global $user;

  // Browse audio.
  
  $form[] = array('#value' => '<div id="header-browse">');

  $form[] = array('#value' => '<h3>'.t('Insert Audio').'</h3>');
  
  $form[] = array('#value' => '</div><div id="header-cancel">');
  $form['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#button_type' => 'button',
    '#attributes' => array('onclick' => 'parent.cancelAction()'),
  );
  $form[] = array('#value' => '</div>');
  
  return $form;
}

// TODO: audio_assist_create_view()

/**
  * Load audio instances and return array of them
 **/
function audio_assist_load_audiofields( $filters=array() ) {
  static $view;

  /*If view doesn't exist, create it*/
  if(!isset($view))
    $view = views_get_view('audio');

  $view->execute(1);

  $result = array();  

  foreach($view->result as $i => $r){
    $field_audio=urlencode($view->render_field('field_arquivo_audio_fid',$i));
    $field_title=$view->render_field('title',$i);
    if($field_audio!='' && $field_title!=''){
      $result[] = array (
	'nid' => $r->nid,
	'fid' => $r->node_data_field_arquivo_audio_field_arquivo_audio_fid,
	'title' => $field_title,
	'audio' => $field_audio,
      );
      if(isset($filters['fid'])){
	$ret = array (
	  'nid' => $r->nid,
  	  'fid' => $r->node_data_field_arquivo_audio_field_arquivo_audio_fid,
  	  'title' => $field_title,
  	  'audio' => $field_audio,
	);
	return $ret;
      }
    }
  }

  return $result;
}



/**
 * Load the thumbnail display pane.
 *
 */
function audio_assist_thumbs() {
  global $user;
    
  $output = '';
  $audios = audio_assist_load_audiofields();
  
  if (!empty($audios)) {
    // Update is put into a hidden field so the javascript can see it.
    $update = (arg(3)) ? 1 : 0;
    
    $output = drupal_get_form('audio_assist_properties_form', $audios , $update);
    
    echo theme('audio_assist_page', $output, array('id' => 'audio_assist_properties', 'onload' => 'parent.initProperties();', 'class' => 'audio_assist'));
  }
  else {
    $output = t('No Audio uploaded yet.');
  }
  
  exit;
}


/**
 * Construct the image properties form.
 */
function audio_assist_properties_form($form_state, &$audios, $update) {
  require_once drupal_get_path('module', 'audio_assist') .'/includes/audio_assist.token.inc';

  // Create the form.
  $list = array();
  foreach($audios as $audio)
    $list[$audio['audio']] = $audio['title'];

  $form[]= array( '#value' => print_r($list), );

  $form['fid'] = array(
    '#type' => 'radios',
    '#title' => t('Audio Nodes'),
    '#default_value' => $audio[0]['audio'],
    '#options' => $list,
  ); 

  // Hidden Fields.
  $form['update'] = array(
    '#type' => 'hidden',
    '#value' => $update,
  );
  $form['insertmode'] = array(
    '#type' => 'hidden',
    '#value' => 'filtertag',
  );
  
  // Buttons.
  $form['buttons'] = array(
    '#prefix' => '<div id="buttons">',
    '#suffix' => '</div>',
  );
  $form['#attributes']['onsubmit'] = 'return parent.insertAudio();';
  $form['buttons']['insert'] = array(
    '#type' => 'submit',
    '#value' => ($update) ? t('Update') : t('Insert'),
    '#attributes' => array('style' => 'float: left;'),
  );
  $form['buttons']['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#button_type' => 'button',
    '#attributes' => array('onclick' => 'return parent.cancelAction();', 'style' => 'float: right;'),
  );
  
  $form['#attributes']['name'] = 'audio_assist';
  
  return $form;
  
}

function audio_assist_properties_form_validate($form, &$form_state) {
  
  $html = audio_assist_render_player($form_state['values']);
  //$html = $form_state['values']['code'];
  audio_assist_set_htmlcode($html);
  drupal_goto('audio_assist/insert_html');
}

/**
 * Store image tag or HTML in session.
 *
 * Used for saving HTML code so it can be inserted instead of the filter tags.
 *
 * @param string $htmlcode
 *   A filter tag or HTML code. If omitted, session variable is emptied.
 *
 * @return string
 *   A previously stored value in the user session.
 */
function audio_assist_set_htmlcode($htmlcode = NULL) {
  if (isset($htmlcode)) {
    $_SESSION['htmlcode'] = urlencode($htmlcode);
  }
  else {
    $html = urldecode($_SESSION['htmlcode']);
    $_SESSION['htmlcode'] = '';
    return $html;
  }
}

function audio_assist_insert_html() {
  $output = drupal_get_form('audio_assist_insert_html_form');
  echo theme('audio_assist_page', $output, array('id' => 'audio_assist_insert_html', 'onload' => 'parent.insertAudio();', 'class' => 'audio_assist'));
  // @todo fix this!
  #drupal_set_message(theme('audio_assist_page', $output, array('id' => 'audio_assist_insert_html', 'onload' => 'parent.insertImage();', 'class' => 'audio_assist')));
  #watchdog('debug_ifa', '<textarea>'. theme('audio_assist_page', $output, array('id' => 'audio_assist_insert_html', 'onload' => 'parent.insertImage();', 'class' => 'audio_assist')) .'</textarea>');
}

function audio_assist_insert_html_form() {
  $htmlcode = audio_assist_set_htmlcode();
  $form[] = array(
    '#id' => 'finalhtmlcode',
    '#type' => 'hidden',
    '#value' => $htmlcode,
  );
  
  $form['insertmode'] = array(
    '#type' => 'hidden',
    '#value' => 'html2',
  );
  
  return $form;
}

/**
 * @} End of "defgroup audio_assist_pages".
 */

/**
 * @defgroup audio_assist_audio Audio Assist embed audio generation
 * @{
 */ 


/**
 * Return audio HTML.
 */
function audio_assist_render_player($attributes = array()) {
  global $user;
  if ($attributes['fid']) {
    $audio = audio_assist_load_audiofields(array('fid' => $attributes['fid']));
    
    if (!$audio) return 'error';
    
    $output = $audio['audio'];
    
    return $output;
  }
}

function audio_assist_popup() {
  $nid = arg(2);
  $node = node_load(array('nid' => $nid));
  drupal_set_title($node->title);
  $size       = variable_get('audio_assist_popup_label', IMAGE_PREVIEW);
  $size       = array('key' => $size);
  $content    = audio_assist_display($node, $size);
  $attributes = array('id' => 'audio_assist_popup');
  
  echo theme('audio_assist_popup', $content, $attributes);
  exit;
}

/**
 * @} End of "defgroup audio_assist_audio".
 */

/**
 * @defgroup audio_assist_macro Image Assist Filter macro parsing
 * @{
 */

/**
 * Return all audio_assist macros as an array.
 * TODO: Again, check following functions if filter is needed.*/
function audio_assist_get_macros($text) {
  $m = array();
  preg_match_all('/ \[ ( [^\[\]]+ )* \] /x', $text, $matches);
  // Don't process duplicates.
  $tag_match = (array) array_unique($matches[1]);
  
  foreach ($tag_match as $macro) {
    $current_macro = '['. $macro .']';
    $param = array_map('trim', explode('|', $macro));
    // The first macro param is assumed to be the function name.
    $func_name = array_shift($param);
    if ($func_name == 'audio_assist') {
      $vars = array();
      foreach ($param as $p) {
        $pos            = strpos($p, '=');
        $varname        = trim(substr($p, 0, $pos));
        $varvalue       = substr($p, $pos + 1);
        $vars[$varname] = trim($varvalue);
      }
      // The full unaltered filter string is the key for the array of filter
      // attributes.
      $m[$current_macro] = $vars;
    }
  }
  
  return $m;
}

/**
 * Determine if audio_assist can render the current page.
 * @see block_list().
 *
 * @return
 *   TRUE if can render, FALSE if not allowed.
 */
function _audio_assist_page_match() {
  $must_match = variable_get('audio_assist_paths_type', 2);
  if ($must_match == 2) {
    return TRUE;
  }
  else {
    $paths  = variable_get('audio_assist_paths', "node/*\ncomment/*");
    $path   = drupal_get_path_alias($_GET['q']);
    $regexp = '/^('. preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1'. variable_get('site_frontpage', 'node') .'\2'), preg_quote($paths, '/')) .')$/';
    $match  = preg_match($regexp, $path);
    return $match != $must_match;
  }
}

/**
 * Determine if audio_assist can render the current textarea.
 * @see block_list().
 *
 * @return
 *   TRUE if can render, FALSE if not allowed.
 */
function _audio_assist_textarea_match($formid) {
  $must_match = variable_get('audio_assist_textareas_type', 2);
  if ($must_match == 2) {
    return TRUE;
  }
  else {
    $formids = variable_get('audio_assist_textareas', "edit-body\nedit-comment");

    $regexp = '/^('. preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/'), array('|', '.*'), preg_quote($formids, '/')) .')$/';
    // Compare with the form id.
    $page_match = preg_match($regexp, $formid);
    // When $must_match has a value of 0, audio_assist is displayed on
    // all pages except those listed in audio_assist_textareas. When set to 1, it
    // is displayed only on those textareas listed in audio_assist_textareas.
    $page_match = !($must_match xor $page_match);

    return $page_match;
  }
}

/**
 * @} End of "defgroup audio_assist_macro".
 */

/**
 * @defgroup audio_assist_theme Audio Assist Theme functions
 * @{
 *
 * @ingroup themeable
 */

function theme_audio_assist_inline($node, $size, $attributes) {
  $caption = '';
  if ($attributes['title'] && $attributes['desc']) {
    $caption = '<strong>'. $attributes['title'] .': </strong>'. $attributes['desc'];
  }
  elseif ($attributes['title']) {
    $caption = '<strong>'. $attributes['title'] .'</strong>';
  }
  elseif ($attributes['desc']) {
    $caption = $attributes['desc'];
  }
  // Change the node title because audio_assist_display() uses the node title for
  // alt and title.
  $node->title = strip_tags($caption);
  $img_tag = audio_assist_display($node, $size);
  
  // Always define an alignment class, even if it is 'none'.
  $output = '<span class="inline inline-'. $attributes['align'] .'">';

  $link = $attributes['link'];
  $url  = '';
  // Backwards compatibility: Also parse link/url in the format link=url,foo.
  if (strpos($link, ',') !== FALSE) {
    list($link, $url) = explode(',', $link, 2);
  }
  elseif (isset($attributes['url'])) {
    $url = $attributes['url'];
  }
  
  if ($link == 'node') {
    $output .= l($img_tag, 'node/'. $node->nid, array('html' => TRUE));
  }
  elseif ($link == 'popup') {
    $popup_size = variable_get('audio_assist_popup_label', IMAGE_PREVIEW);
    $info       = image_get_info(file_create_path($node->images[$popup_size]));
    $width      = $info['width'];
    $height     = $info['height'];
    $popup_url  = file_create_url($node->images[variable_get('audio_assist_popup_label', IMAGE_PREVIEW)]);
    $output .= l(
      $img_tag,
      $popup_url,
      array(
        'attributes' =>
          array('onclick' => "launch_popup({$node->nid}, {$width}, {$height}); return false;",
                'target' => '_blank'),
        'html' => TRUE)
      );
  }
  elseif ($link == 'url') {
    $output .= l($img_tag, $url, array('html' => TRUE));
  }
  else {
    $output .= $img_tag;
  }
  
  if ($caption) {
    if ($attributes['align'] != 'center') {
      $info = image_get_info(file_create_path($node->images[$size['key']]));
      // Reduce the caption width slightly so the variable width of the text
      // doesn't ever exceed image width.
      $width = $info['width'] - 2;
      $output .= '<span class="caption" style="width: '. $width .'px;">'. $caption .'</span>';
    }
    else {
      $output .= '<span class="caption">'. $caption .'</span>';
    }
  }
  $output .= '</span>';
  return $output;
}

function theme_audio_assist_filter($text) {
  // The div tag added to the end of each node is necessary to clear the
  // floating properties of inline images immediately after a node's content.
  return $text .'<div class="image-clear"></div>';
}

function theme_audio_assist_popup($content, $attributes = NULL) {
  $title = drupal_get_title();
  $output  = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'."\n";
  $output .= "<html>\n";
  $output .= "<head>\n";
  $output .= '<title>'. $title ."</title>\n";
  $output .= drupal_get_html_head();
  $output .= drupal_get_css();
  $output .= "</head>\n";
  $output .= '<body'. drupal_attributes($attributes) .">\n";
  $output .= "<!-- begin content -->\n";
  $output .= l($content, '', array('attributes' => array('onclick' => 'javascript:window.close();'), 'html' => TRUE));
  $output .= "<!-- end content -->\n";
  $output .= '</body>';
  $output .= '</html>';
  return $output;
}

function theme_audio_assist_page($content, $attributes = NULL) {
  $title = drupal_get_title();
  $output = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
  $output .= '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">'."\n";
  $output .= "<head>\n";
  $output .= '<title>'. $title ."</title>\n";
  
  // Note on CSS files from Benjamin Shell:
  // Stylesheets are a problem with image assist. Image assist works great as a
  // TinyMCE plugin, so I want it to LOOK like a TinyMCE plugin. However, it's
  // not always a TinyMCE plugin, so then it should like a themed Drupal page.
  // Advanced users will be able to customize everything, even TinyMCE, so I'm
  // more concerned about everyone else. TinyMCE looks great out-of-the-box so I
  // want image assist to look great as well. My solution to this problem is as
  // follows:
  // If this image assist window was loaded from TinyMCE, then include the
  // TinyMCE popups_css file (configurable with the initialization string on the
  // page that loaded TinyMCE). Otherwise, load drupal.css and the theme's
  // styles. This still leaves out sites that allow users to use the TinyMCE
  // plugin AND the Add Image link (visibility of this link is now a setting).
  // However, on my site I turned off the text link since I use TinyMCE. I think
  // it would confuse users to have an Add Images link AND a button on the
  // TinyMCE toolbar.
  // 
  // Note that in both cases the audio_assist.css file is loaded last. This
  // provides a way to make style changes to audio_assist independently of how it
  // was loaded.
  $output .= drupal_get_html_head();
  $output .= drupal_get_js();
  $output .= "\n<script type=\"text/javascript\"><!-- \n";
  $output .= "  if (parent.tinyMCE && parent.tinyMCEPopup && parent.tinyMCEPopup.getParam('popups_css')) {\n";
  $output .= "    document.write('<link href=\"' + parent.tinyMCEPopup.getParam('popups_css') + '\" rel=\"stylesheet\" type=\"text/css\">');\n";
  $output .= "  } else {\n";
  foreach (drupal_add_css() as $media => $type) {
    $paths = array_merge($type['module'], $type['theme']);
    foreach (array_keys($paths) as $path) {
      // Don't import audio_assist.css twice.
      if (!strstr($path, 'audio_assist.css')) {
        $output .= "  document.write('<style type=\"text/css\" media=\"{$media}\">@import \"". base_path() . $path ."\";<\/style>');\n";
      }
    }
  }
  $output .= "  }\n";
  $output .= "--></script>\n";
  // Ensure that audio_assist.js is imported last.
  $path = drupal_get_path('module', 'audio_assist') .'/audio_assist_popup.css';
  $output .= "<style type=\"text/css\" media=\"all\">@import \"". base_path() . $path ."\";</style>\n";
  
  $output .= "</head>\n";
  $output .= '<body'. drupal_attributes($attributes) .">\n";
  
  $output .= theme_status_messages();
  
  $output .= "<!-- begin content -->\n";
  $output .= $content;
  $output .= "<!-- end content -->\n";
  $output .= '</body>';
  $output .= '</html>';
  return $output;
}

/**
 * @} End of "defgroup audio_assist_theme".
 */

/**
 * Implementation of hook_wysiwyg_plugin().
 */
function audio_assist_wysiwyg_plugin($editor, $version) {
  switch ($editor) {
    case 'tinymce':
      if ($version > 3) {
        return array(
          'audio_assist' => array(
            'path' => drupal_get_path('module', 'audio_assist') .'/drupalimage/editor_plugin.js',
            'buttons' => array('audio_assist' => t('Audio Assist')),
            'url' => 'http://drupal.org/project/audio_assist',
            'extended_valid_elements' => array('img[class|src|border=0|alt|title|width|height|align|name|style]'),
            'load' => TRUE,
          ),
        );
      }
      break;
  }
}



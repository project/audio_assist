-- SUMMARY --

Audio Assist is a javascript assistant to add embed audio from audio nodes
(using filefield, views and swftools) to any textarea/tinyMCE instance. 

For a full description visit the project page:
  http://drupal.org/project/audio_assist
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/audio_assist


-- REQUIREMENTS --

* Views module <http://drupal.org/project/views>

* File Field  module <http://drupal.org/project/filefield>

* SWF Tools module <http://drupal.org/project/swftools>

-- INSTALLATION --

Install as usual. See configuration section for more information.


-- CONFIGURATION --

TODO: review this.

//In order to upload files, enable Content Copy module from CCK bundle, then use the contents of the ifa_upload_content_type.php file in the import form, using the name ifa_upload for the content type.

Also, enable the SWF filter in the desirede Input type.

Add the Inline Images filter to the desired Input Formats for better use of the module.
